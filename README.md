# SimpleRover #

This project is the brain of my mini rover built with the following parts:

- Chassi Bogie Runt Rover of Actobotics
- 7.2V Battery Pack
- L298N Motor drive
- Raspberry PI 3 with Windows IoT

Click [here](https://flic.kr/s/aHskLSQ3eS) for rover pictures.

#Keywords:
- UWP
- Visual C#
- Visual Studio 2015
- Windows IoT