﻿using PwmSoftware;
using SimpleRover.Config;
using SimpleRover.Utility;
using Windows.Devices.Gpio;
using Windows.Devices.Pwm.Provider;

namespace SimpleRover.RoverUtility.MotorControl
{
    class PwmMotor : IMotor
    {
        //Config
        private float _threshold = 0.0f;

        //Status
        private bool _enabled = false;
        private int _enablePin = 0;
        private float _potential = 0.0f;
        private bool _active = false;

        //Controller
        private IPwmControllerProvider _pwmController = null;

        //Control pins
        private GpioPin _pinIn1 = null;
        private GpioPin _pinIn2 = null;

        public PwmMotor(int enablePin, int pinIn1, int pinIn2)
        {
            //Loading config
            OnLoadConfig();
            //Setting pins
            _enablePin = enablePin;
            var gpioController = GpioController.GetDefault();
            if (gpioController != null)
            {
                //Initializing pin enable
                _pwmController = PwmProviderSoftware.GetPwmProvider().GetControllers()[0];
                _pwmController.SetDesiredFrequency(50);
                _pwmController.AcquirePin(_enablePin);
                //Initializing pin 1
                _pinIn1 = gpioController.OpenPin(pinIn1);
                _pinIn1.SetDriveMode(GpioPinDriveMode.Output);
                _pinIn1.Write(GpioPinValue.Low);
                //Initializing pin 2
                _pinIn2 = gpioController.OpenPin(pinIn2);
                _pinIn2.SetDriveMode(GpioPinDriveMode.Output);
                _pinIn2.Write(GpioPinValue.Low);
            }
            //Handling reload message
            Messenger.Default.RegisterNotification<MessageConfigReload>(OnReloadConfig);
            //Activating config
            _active = true;
            //Updating pins
            Update();
        }

        ~PwmMotor()
        {
            _pwmController?.ReleasePin(_enablePin);
        }

        #region Auxilliary
        private bool Update()
        {
            var ret = false;
            if (_active)
            {
                //Setting enabled/disabled
                if (_enabled)
                {
                    var dutyCycle = (_potential >= 0 ? _potential : -_potential);
                    //Setting enable
                    _pwmController?.EnablePin(_enablePin);
                    //Setting duty cycle
                    _pwmController?.SetPulseParameters(_enablePin, dutyCycle, false);
                    ret = true;
                }
                else
                {
                    _pwmController?.DisablePin(_enablePin);
                }
                //Adjusting control pins
                if (_potential > _threshold)
                {
                    _pinIn1?.Write(GpioPinValue.High);
                    _pinIn2?.Write(GpioPinValue.Low);
                }
                else if (_potential < -_threshold)
                {
                    _pinIn1?.Write(GpioPinValue.Low);
                    _pinIn2?.Write(GpioPinValue.High);
                }
                else
                {
                    _pinIn1?.Write(GpioPinValue.Low);
                    _pinIn2?.Write(GpioPinValue.Low);
                }
            }

            return ret;
        }
        #endregion

        #region Configuratio
        private void OnLoadConfig()
        {
            var config = Configuration.Default;
            //Normalizing threshold
            if (config.MotorThreshold >= 0.0f && config.MotorThreshold < 1.0f)
            {
                _threshold = config.MotorThreshold;
            }
        }

        private void OnReloadConfig(object o)
        {
            _active = false;
            OnLoadConfig();
            _active = true;
            Update();
        }
        #endregion

        public float GetPotential()
        {
            return _potential;
        }

        public bool IsEnabled()
        {
            return _enabled;
        }

        public bool IsForward()
        {
            return _potential > 0;
        }

        public bool SetEnabled(bool enabled)
        {
            _enabled = enabled;

            return Update();
        }

        public bool SetPotential(float potential)
        {
            if (potential < -1.0f)
            {
                _potential = -1.0f;
            }
            else if (potential > 1.0f)
            {
                _potential = 1.0f;
            }
            else
            {
                _potential = potential;
            }

            return Update();
        }
    }
}
