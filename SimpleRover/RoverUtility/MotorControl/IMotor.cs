﻿namespace SimpleRover.RoverUtility.MotorControl
{
    interface IMotor
    {
        //Returns enabled/disabled
        bool IsEnabled();

        //Returns if Forwaded
        bool IsForward();

        //Return its potential in -1.0f to 1.0f
        float GetPotential();

        //Enable/Disable the motor
        bool SetEnabled(bool enabled);

        //Float value between -1.0f (Backward) to 1.0f (Forward)
        bool SetPotential(float potential);
    }
}
