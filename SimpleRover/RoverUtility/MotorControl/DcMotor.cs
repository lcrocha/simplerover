﻿using SimpleRover.Config;
using SimpleRover.Utility;
using System.Threading.Tasks;
using Windows.Devices.Gpio;

namespace SimpleRover.RoverUtility.MotorControl
{
    class DcMotor : IMotor
    {
        //Config
        private float _threshold = 0.0f;

        //Status
        private bool _enabled = false;
        private float _potential = 0.0f;
        private bool _active = false;

        //Pins
        private GpioPin _pinEnable = null;
        private GpioPin _pinIn1 = null;
        private GpioPin _pinIn2 = null;

        public DcMotor(int enablePin, int pinIn1, int pinIn2)
        {
            OnLoadConfig();
            //Initializing gpio
            var gpioController = GpioController.GetDefault();
            if (gpioController != null)
            {
                //Initializing pin enable
                _pinEnable = gpioController.OpenPin(enablePin);
                _pinEnable.SetDriveMode(GpioPinDriveMode.Output);
                _pinEnable.Write(GpioPinValue.Low);
                //Initializing pin 1
                _pinIn1 = gpioController.OpenPin(pinIn1);
                _pinIn1.SetDriveMode(GpioPinDriveMode.Output);
                _pinIn1.Write(GpioPinValue.Low);
                //Initializing pin 2
                _pinIn2 = gpioController.OpenPin(pinIn2);
                _pinIn2.SetDriveMode(GpioPinDriveMode.Output);
                _pinIn2.Write(GpioPinValue.Low);
            }
            //Setting reload message
            Messenger.Default.RegisterNotification<MessageConfigReload>(OnReloadConfig);
            //Activating motor
            _active = true;
            //Update
            Update();
        }

        ~DcMotor()
        {
        }

        #region Auxilliary
        private bool Update()
        {
            var ret = false;
            if (_active)
            {
                //Setting pins
                _pinEnable?.Write(_enabled ? GpioPinValue.High : GpioPinValue.Low);
                _pinIn1?.Write(_potential >= 0.0f ? GpioPinValue.High : GpioPinValue.Low);
                _pinIn2?.Write(_potential >= 0.0f ? GpioPinValue.Low : GpioPinValue.High);
                ret = true;
            }

            return ret;
        }
        #endregion

        #region Configuration
        private void OnLoadConfig()
        {
            var config = Configuration.Default;
            //Normalizing threshold
            if (config.MotorThreshold >= 0.0f && config.MotorThreshold < 1.0f)
            {
                _threshold = config.MotorThreshold;
            }
        }

        private void OnReloadConfig(object o)
        {
            _active = false;
            OnLoadConfig();
            _active = true;
            Update();
        }
        #endregion

        #region Operations
        public bool IsEnabled()
        {
            return _enabled;
        }

        public bool IsForward()
        {
            return GetPotential() >= 0.0f;
        }

        public float GetPotential()
        {
            return _potential;
        }

        public bool SetEnabled(bool enabled)
        {
            _enabled = enabled;

            return Update();
        }

        public bool SetPotential(float potential)
        {
            //Normalizing potential
            if(potential < -_threshold)
            {
                potential = -1.0f;
            }
            else if(potential > _threshold)
            {
                potential = 1.0f;
            }
            else
            {
                potential = 0.0f;
                _enabled = false;
            }
            _potential = potential;

            return Update();
        }
        #endregion
    };
}
