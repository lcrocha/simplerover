﻿using SimpleRover.Config;
using SimpleRover.RoverUtility.MotorControl;
using SimpleRover.Utility;
using System;
using System.Diagnostics;

namespace SimpleRover.RoverUtility
{
    public interface IRoverController
    {
        /// <summary>
        /// Event to singnalizing update on data
        /// </summary>
        /// <returns></returns>
        event EventHandler DataChanged;

        //Checks if it is Enabled
        bool IsEnabled();

        //Gets Left motor potential
        float GetLeftPotential();

        //Gets Right motor potential
        float GetRightPotential();

        //Set motors enabled/disable
        bool SetEnabled(bool enabled);

        //Set Movement
        bool SetMovement(float movementX, float movementY);
    };

    public class RoverController : IRoverController
    {
        //Status
        private bool _active = false;

        //Motors
        private IMotor _motorLeft = null;
        private IMotor _motorRight = null;

        //Controls
        private bool _enabled = false;

        //Movement
        private float _movementX = 0.0f;
        private float _movementY = 0.0f;

        //Events
        public event EventHandler DataChanged;

        //Config
        private float _motorThreshold = 0.0f;

        #region Instancing
        private static RoverController _instance = null;
        public static IRoverController Default
        {
            get
            {
                return _instance ?? (_instance = new RoverController());
            }
        }

        private RoverController()
        {
            //Loading config
            OnLoadConfig();
            //Activating operation
            _active = true;
            //Update data
            Update();
            //Handling reload message
            Messenger.Default.RegisterNotification<MessageConfigReload>(OnReloadConfig);
        }
        #endregion

        #region Config
        private void OnLoadConfig()
        {
            var motorMode = Configuration.Default.MotorMode;
            if (motorMode == MotorMode.PwmMotor)
            {
                _motorLeft = new PwmMotor(2, 3, 4);
                _motorRight = new PwmMotor(17, 27, 22);
            }
            else
            {
                _motorLeft = new DcMotor(2, 3, 4);
                _motorRight = new DcMotor(17, 27, 22);
            }
            _motorThreshold = Configuration.Default.MotorThreshold;
        }

        private void OnReloadConfig(object o)
        {
            //Deactivating operation
            _active = false;
            //Disposing motors
            _motorLeft = null;
            _motorRight = null;
            //Loading config
            OnLoadConfig();
            //Activating operation
            _active = true;
            //Udating data
            Update();
        }
        #endregion

        #region Auxilliary
        private float NormalizeValue(float value)
        {
            if(value < -1.0f)
            {
                return -1.0f;
            }
            else if(value > 1.0f)
            {
                return 1.0f;
            }
            else if(value > -_motorThreshold && value < _motorThreshold)
            {
                return 0.0f;
            }

            return value;
        }

        private bool Update()
        {
            //Enabling motors
            _motorLeft.SetEnabled(_enabled);
            _motorRight.SetEnabled(_enabled);

            //Calculating potentials
            var potentialLeft = CalculatePotentialLeft();
            var potentialRight = CalculatePotentialRight();
            //Setting potentials
            _motorLeft.SetPotential(potentialLeft);
            _motorRight.SetPotential(potentialRight);
            //Sending event
            DataChanged?.Invoke(this, null);

            return true;
        }

        private float CalculatePotential(float signal)
        {
            var theta = Math.Atan2(_movementY, _movementX);
            var r = Math.Sqrt(_movementX * _movementX + _movementY * _movementY);

            return NormalizeValue((float)(r));
        }

        private float CalculatePotentialLeft()
        {
            return CalculatePotential(-1.0f);
        }

        private float CalculatePotentialRight()
        {
            return -CalculatePotential(1.0f);
        }
        #endregion

        #region Status
        public bool IsEnabled()
        {
            return _enabled;
        }

        public float GetLeftPotential()
        {
            return _motorLeft.GetPotential();
        }

        public float GetRightPotential()
        {
            return _motorRight.GetPotential();
        }
        #endregion

        #region Operations
        public bool SetEnabled(bool enabled)
        {
            var ret = false;
            if (_active)
            {
                _enabled = enabled;
                ret = Update();
            }

            return ret;
        }

        public bool SetMovement(float movementX, float movementY)
        {
            var ret = false;
            if (_active)
            {
                _movementX = NormalizeValue(movementX);
                _movementY = NormalizeValue(movementY);
                ret = Update();
            }

            return ret;
        }
        #endregion

        /* switch (_roverMovement)
         {
             case RoverMovement.Forward:
                 _motorLeft.SetPotential(1.0f);
                 _motorRight.SetPotential(-1.0f);
                 break;
             case RoverMovement.Backward:
                 _motorLeft.SetPotential(-1.0f);
                 _motorRight.SetPotential(1.0f);
                 break;
             case RoverMovement.Right:
                 _motorLeft.SetPotential(-1.0f);
                 _motorRight.SetPotential(-1.0f);
                 break;
             case RoverMovement.Left:
                 _motorLeft.SetPotential(1.0f);
                 _motorRight.SetPotential(1.0f);
                 break;*/
    }
}
