﻿using System;
using Windows.ApplicationModel;
using Windows.ApplicationModel.Activation;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Navigation;
using Windows.System;
using SimpleRover.RoverUtility;
using Windows.Gaming.Input;
using System.Threading;
using SimpleRover.Config;
using System.Diagnostics;
using SimpleRover.Utility;

namespace SimpleRover
{
    /// <summary>
    /// Provides application-specific behavior to supplement the default Application class.
    /// </summary>
    sealed partial class App : Application
    {
        private IRoverController _roverController = null;
        private Timer _gamepadReadingTimer = null;
        private bool _gamepadReadingActivated = false;
        private int _gamepadReadingPeriod = 0;
        /// <summary>
        /// Initializes the singleton application object.  This is the first line of authored code
        /// executed, and as such is the logical equivalent of main() or WinMain().
        /// </summary>
        public App()
        {
            Microsoft.ApplicationInsights.WindowsAppInitializer.InitializeAsync(
                Microsoft.ApplicationInsights.WindowsCollectors.Metadata |
                Microsoft.ApplicationInsights.WindowsCollectors.Session);
            this.InitializeComponent();

            this.Suspending += OnSuspending;

        }

        private void OnLoadConfig()
        {
            _gamepadReadingPeriod = Configuration.Default.GamepadReadingPeriod;
        }

        private void OnReloadConfig(object o)
        {
            _gamepadReadingTimer.Change(Timeout.Infinite, Timeout.Infinite);
            OnLoadConfig();
            if (_gamepadReadingActivated)
            {
                _gamepadReadingTimer.Change(_gamepadReadingPeriod, Timeout.Infinite);
            }
        }

        private void Gamepad_GamepadRemoved(object sender, Gamepad e)
        {
            if(Gamepad.Gamepads.Count == 0 && _gamepadReadingActivated)
            {
                Debug.WriteLine("Gamepad removed");
                _gamepadReadingTimer.Change(Timeout.Infinite, Timeout.Infinite);
                _gamepadReadingActivated = false;
            }
        }

        private void Gamepad_GamepadAdded(object sender, Gamepad e)
        {
            if (Gamepad.Gamepads.Count > 0 && !_gamepadReadingActivated)
            {
                Debug.WriteLine("Gamepad added");
                _gamepadReadingTimer.Change(_gamepadReadingPeriod, Timeout.Infinite);
                _gamepadReadingActivated = true;
            }
        }

        private void OnGamepadReading(object o)
        {
            if(_gamepadReadingActivated)
            {
                foreach (var gamepad in Gamepad.Gamepads)
                {
                    var reading = gamepad.GetCurrentReading();
                    //Setting enable/disable
                    if (reading.Buttons == GamepadButtons.A)
                    {
                        _roverController.SetEnabled(!_roverController.IsEnabled());
                    }
                    //Reading joystick
                    _roverController.SetMovement((float)reading.LeftThumbstickX, (float)reading.LeftThumbstickY);

                }
                //Rescheduling reading timer
                _gamepadReadingTimer.Change(_gamepadReadingPeriod, Timeout.Infinite);
            }
        }

        private void OnKeyDown(Windows.UI.Core.CoreWindow sender, Windows.UI.Core.KeyEventArgs args)
        {
            var virtualKey = args.VirtualKey;
            if (virtualKey == VirtualKey.Enter)
            {
                _roverController.SetEnabled(!_roverController.IsEnabled());
            }
            else if (virtualKey == VirtualKey.Left)
            {
                _roverController.SetMovement(-1.0f, 0.0f);
            }
            else if (virtualKey == VirtualKey.Right)
            {
                _roverController.SetMovement(1.0f, 0.0f);
            }
            else if (virtualKey == VirtualKey.Up)
            {
                _roverController.SetMovement(0.0f, 1.0f);
            }
            else if (virtualKey == VirtualKey.Down)
            {
                _roverController.SetMovement(0.0f, -1.0f);
            }
        }

        /// <summary>
        /// Invoked when the application is launched normally by the end user.  Other entry points
        /// will be used such as when the application is launched to open a specific file.
        /// </summary>
        /// <param name="e">Details about the launch request and process.</param>
        protected override void OnLaunched(LaunchActivatedEventArgs e)
        {
#if DEBUG
            if (System.Diagnostics.Debugger.IsAttached)
            {
                this.DebugSettings.EnableFrameRateCounter = true;
            }
#endif
            //Initializing SimpleRover data
            _roverController = RoverController.Default;
            //Loading config
            OnLoadConfig();
            //Mapping events
            Window.Current.CoreWindow.KeyDown += OnKeyDown;
            Gamepad.GamepadAdded += Gamepad_GamepadAdded;
            Gamepad.GamepadRemoved += Gamepad_GamepadRemoved;
            //Setting timer
            _gamepadReadingTimer = new Timer(OnGamepadReading, this, Timeout.Infinite, Timeout.Infinite);
            Gamepad_GamepadAdded(null, null);
            //Handling reload message
            Messenger.Default.RegisterNotification<MessageConfigReload>(OnReloadConfig);

            //Default implementation
            Frame rootFrame = Window.Current.Content as Frame;

            // Do not repeat app initialization when the Window already has content,
            // just ensure that the window is active
            if (rootFrame == null)
            {
                // Create a Frame to act as the navigation context and navigate to the first page
                rootFrame = new Frame();

                rootFrame.NavigationFailed += OnNavigationFailed;

                if (e.PreviousExecutionState == ApplicationExecutionState.Terminated)
                {
                    //TODO: Load state from previously suspended application
                }

                // Place the frame in the current Window
                Window.Current.Content = rootFrame;
            }

            if (e.PrelaunchActivated == false)
            {
                if (rootFrame.Content == null)
                {
                    // When the navigation stack isn't restored navigate to the first page,
                    // configuring the new page by passing required information as a navigation
                    // parameter
                    rootFrame.Navigate(typeof(MainPage), e.Arguments);
                }
                // Ensure the current window is active
                Window.Current.Activate();
            }
        }

        /// <summary>
        /// Invoked when Navigation to a certain page fails
        /// </summary>
        /// <param name="sender">The Frame which failed navigation</param>
        /// <param name="e">Details about the navigation failure</param>
        void OnNavigationFailed(object sender, NavigationFailedEventArgs e)
        {
            throw new Exception("Failed to load Page " + e.SourcePageType.FullName);
        }

        /// <summary>
        /// Invoked when application execution is being suspended.  Application state is saved
        /// without knowing whether the application will be terminated or resumed with the contents
        /// of memory still intact.
        /// </summary>
        /// <param name="sender">The source of the suspend request.</param>
        /// <param name="e">Details about the suspend request.</param>
        private void OnSuspending(object sender, SuspendingEventArgs e)
        {
            var deferral = e.SuspendingOperation.GetDeferral();
            //TODO: Save application state and stop any background activity
            deferral.Complete();
        }
    }
}
