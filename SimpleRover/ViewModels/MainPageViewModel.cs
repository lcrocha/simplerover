﻿using SimpleRover.RoverUtility;
using SimpleRover.Utility;
using System.ComponentModel;
using Windows.UI.Xaml;
using System;
using Windows.UI.Core;
using Windows.ApplicationModel.Core;

namespace SimpleRover.ViewModels
{
    class MainPageViewModel : INotifyPropertyChanged
    {
        private IRoverController _roverController = null;
        public event PropertyChangedEventHandler PropertyChanged;

        public MainPageViewModel()
        {
            _roverController = RoverController.Default;
            _roverController.DataChanged += RoverController_DataChanged;
        }

        #region Auxilliary
        protected void OnPropertyChanged(string propertyName)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }

        private void OnUpdateData()
        {
            OnPropertyChanged("IsMotorsEnabled");
            OnPropertyChanged("LeftPotential");
            OnPropertyChanged("RightPotential");
        }
        #endregion

        #region Properties
        public bool IsMotorsEnabled
        {
            get
            {
                return (_roverController != null ? _roverController.IsEnabled() : false);
            }
        }

        public float LeftPotential
        {
            get
            {
                return _roverController.GetLeftPotential();
            }
        }

        public float RightPotential
        {
            get
            {
                return _roverController.GetRightPotential();
            }
        }
        #endregion

        private void RoverController_DataChanged(object sender, EventArgs e)
        {
            var dispatcher = CoreApplication.MainView.CoreWindow.Dispatcher;
            if(dispatcher != null)
            {
                var res = dispatcher.RunAsync(CoreDispatcherPriority.Normal, OnUpdateData);
            }
        }
    }
}