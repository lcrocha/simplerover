﻿namespace SimpleRover.Config
{
    /// <summary>
    /// Message to signalizing a reload config is necessary
    /// </summary>
    public class MessageConfigReload
    { }

    enum MotorMode
    {
        DcMotor,
        PwmMotor
    };
    
    class Configuration
    {
        /// <summary>
        /// Threshold for motor movement
        /// </summary>
        public float MotorThreshold = 0.2f;

        /// <summary>
        /// Period for checking gamepad state
        /// </summary>
        public int GamepadReadingPeriod = 100;

        /// <summary>
        /// Motor mode [DcMotor / PwmMotor]
        /// </summary>
        public MotorMode MotorMode = MotorMode.PwmMotor;

        #region Instancing
        private static Configuration _instance = null;

        public static Configuration Default
        {
            get
            {
                return (_instance ?? (_instance = new Configuration()));
            }
        }

        private Configuration()
        {

        }
        #endregion
    }
}
