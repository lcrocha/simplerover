﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SimpleRover.Utility
{
    public interface IMessenger
    {
        /// <summary>
        /// Register a query to its callback
        /// </summary>
        /// <typeparam name="TMessage">Type of query</typeparam>
        /// <param name="func">Callback function</param>
        void RegisterQuery<TMessage>(Func<object, object> func);

        /// <summary>
        /// Executes a query to its listeners
        /// </summary>
        /// <typeparam name="TMessage">Type of query</typeparam>
        /// <param name="data">User data</param>
        /// <param name="result">Result of query</param>
        /// <returns>There is a result available</returns>
        bool DoQuery<TMessage>(object data, ref object result);

        /// <summary>
        /// Unregister the query
        /// </summary>
        /// <typeparam name="TMessage">Type of query</typeparam>
        void UnregisterQuery<TMessage>();

        /// <summary>
        /// Register a notification
        /// </summary>
        /// <typeparam name="TMessage">Type of notification</typeparam>
        /// <param name="action">Callback action</param>
        void RegisterNotification<TMessage>(Action<object> action);

        /// <summary>
        /// Executes a notification
        /// </summary>
        /// <typeparam name="TMessage">Type of notification</typeparam>
        /// <param name="data">User data</param>
        /// <returns>Quantity of listeners executed</returns>
        int DoNotification<TMessage>(object data);
        void UnregisterNotification<TMessage>(Action<object> action);
    }

    public class Messenger : IMessenger
    {
        #region Instancing
        private static Messenger _instance = null;
        public static IMessenger Default
        {
            get
            {
                if(_instance == null)
                {
                    lock (_creationLock)
                    {
                        _instance = new Messenger();
                    }
                }

                return _instance;
            }
        }
        private static readonly object _creationLock = new object();

        private Messenger()
        {

        }
        #endregion

        #region Query
        private readonly ConcurrentDictionary<Type, Func<object, object>> _queries = new ConcurrentDictionary<Type, Func<object, object>>();

        public void RegisterQuery<TMessage>(Func<object, object> func)
        {
            _queries.AddOrUpdate(typeof(TMessage), func, (key, existing) => { return func; });
        }

        public void UnregisterQuery<TMessage>()
        {
            Func<object, object> func = null;
            _queries.TryRemove(typeof(TMessage), out func);
        }

        public bool DoQuery<TMessage>(object data, ref object result)
        {
            Func<object, object> func = null;
            if (_queries.TryGetValue(typeof(TMessage), out func))
            {
                result = func(data);
                return true;
            }

            return false;
        }
        #endregion

        #region Notification
        private readonly ConcurrentDictionary<Type, List<Action<object>>> _notifications = new ConcurrentDictionary<Type, List<Action<object>>>();

        public void RegisterNotification<TMessage>(Action<object> action)
        {
            _notifications.AddOrUpdate(
                typeof(TMessage),
                new List<Action<object>>() { action },
                (key, actions) =>
                {
                    actions.Add(action);

                    return actions;
                });
        }

        public void UnregisterNotification<TMessage>(Action<object> action)
        {
            List<Action<object>> actions = null;
            if (_notifications.TryGetValue(typeof(TMessage), out actions))
            {
                actions.Remove(action);
                if (actions.Count == 0)
                {
                    _notifications.TryRemove(typeof(TMessage), out actions);
                }
            }
        }

        public int DoNotification<TMessage>(object data)
        {
            var ret = 0;
            List<Action<object>> actions = null;
            if (_notifications.TryGetValue(typeof(TMessage), out actions))
            {
                foreach (var action in actions)
                {
                    action(data);
                    ret++;
                }
            }

            return ret;
        }
        #endregion
    }
}